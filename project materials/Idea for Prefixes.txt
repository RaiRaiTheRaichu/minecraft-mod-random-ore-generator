Prefixes and suffixes:

Type: Color, hardness, etc.


interface Prefix
{
    public void applyPrefix(Block block)
}

Public class ColorPrefix implements Prefix
{
    int colorCode; // or something...
    
    public ColorPrefix(int colorCode)
    {
        this.colorCode = colorCode;
    }
    
    @Override
    public void applyPrefix(Block block)
    {
        // change the color of the block
    }
}

Public class HardnessPrefix implements Prefix
{
    double hardnessMultiplier; // or something...
    
    public HardnessPrefix(double hardnessMultiplier)
    {
        this.hardnessMultiplier = hardnessMultiplier;
    }
    
    @Override
    public void applyPrefix(Block block)
    {
        // apply hardness multiplier to block
    }
}

public class PrefixSupplier
{
    private static final PrefixSupplier supplier = new PrefixSupplier();
    private ArrayList<Prefix> prefixList;
    
    private PrefixSupplier()
    {
        // ...read prefix file, create prefixes. PrefixLoader class that returns a list?
    }
    
    public static PrefixSupplier getInstance()
    {
        return supplier;
    }
    
    public getRandomPrefix()
    {
        // ...return random prefix
    }
    
    
}