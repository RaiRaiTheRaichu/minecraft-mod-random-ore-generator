#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <random>
#include <stdlib.h>
#include <time.h>

using namespace std;

string prefix;
string suffix;
string randomOre1; //Assuming this is the class file for ore #1. This will be the full name once we get our suffix and prefix.

vector<string> listPrefixColor(3); //To do: Separate them by individual colors - that way we can make sure that randomOre2 and so on doesn't pick the same color. We don't want two ores with incredibly similar colors.
vector<string> listPrefixDura(3);
vector<string> listPrefixStd(5);
vector<string> listSuffix(10);

//There are going to be tons of more prefix types, including if it's using the ore or crystal texture, if it's a fuel source, if it glows in the dark, the enchantability being higher, etc.

int main() {

	//Populate vector with strings from a file
	//For now, the vectors are manually populated

	listPrefixColor.at(0) = "Blu"; 
	
	/* Idea: Define the tables of prefixes in a totally separate file and have the block class files import it. 
	Maybe then I can manually specify in that other file that if it uses Blu-, it overrides the RGB value to a specific color I 
	define in that file. The only thing I can think of otherwise with my knowledge is setting down below where it would theoretically define RGB values, 
	just check if the prefix is Blu and manually set it. Maybe there's a cleaner way though, that if it chooses index 0, import the color profile I manually 
	set in the prefix definition file. (hope that makes sense) */
	
	listPrefixColor.at(1) = "Ochr";
	listPrefixColor.at(2) = "Taup";

	listPrefixDura.at(0) = "Wolfr";
	listPrefixDura.at(1) = "Adamant";
	listPrefixDura.at(2) = "Dur";

	listPrefixStd.at(0) = "Ferr";
	listPrefixStd.at(1) = "Cupr";
	
	/* Interesting note here, cupr- means copper-based and ferr- would refer to iron based. Forge has a built in API called the ore dictionary that if two ores added by 
	different mods have the same "dictionary name" set, they can both be used interchangeably. Maybe add a check somewhere (in the other file I was talking about just above,
	maybe) that if it lands on the index for Cupr, it imports a profile where it sets the ore dictionary name to "copper" so it can be compatible with recipes in the game 
	that use copper metals. */
	
	listPrefixStd.at(2) = "Compend";
	listPrefixStd.at(3) = "Uwun"; //I'm making uwunium happen
	listPrefixStd.at(4) = "Polon";

	listSuffix.at(0) = "ite";
	listSuffix.at(1) = "ium";
	listSuffix.at(2) = "alumen";
	listSuffix.at(3) = "oxide"; //Not sure on this one
	listSuffix.at(4) = "ine";
	listSuffix.at(5) = "ox";
	listSuffix.at(6) = "onium";
	listSuffix.at(7) = "ide";
	listSuffix.at(8) = "ate"; //Not sure on this one either
	listSuffix.at(9) = "ode";
	
	/* I'm going to look up a table of all known alloys and metals and take some prefixes and suffixes from that to make a dictionary. Just give me a format to use that can 
	be loaded and imported easily. If you can make a template file in Eclipse for me, I can populate it on my own time with the proper categories. */


	srand(time(NULL));
	/* Instead of using time(NULL), we're going to set it to the world seed that's generated when the game... generates the world. Also, should add support for overriding the seed with a custom seed 
	with a seed set in a configuration file*/

	int maxOresGen; 
	/* Useless in this program, but this could be used to define how many random ores to generate (default value 5? allow people to set the value in config? add limit of, say, 50, so people can't 
	fuck their game up with hundreds of ores?). I don't know if we need a separate class for each block or if we can do them all in one class. We'll have to test that out. */


	string randOre1Prefix; //The actually chosen names.
	string randOre1Suffix;
	
	int loop = 1; //Debugging, allows us to keep cycling through some names.
	
	while (loop == 1) {

		//Picking a prefix.
		int randomNumPrefix;
		randomNumPrefix = rand() % 3; //Random number between 0-2, as we have 3 vectors to pick from.
		if (randomNumPrefix == 0) {
			randOre1Prefix = listPrefixColor.at(rand() % (listPrefixColor.size() - 1));
		}
		if (randomNumPrefix == 1) {
			randOre1Prefix = listPrefixDura.at(rand() % (listPrefixDura.size() - 1));
		}
		if (randomNumPrefix == 2) {
			randOre1Prefix = listPrefixStd.at(rand() % (listPrefixStd.size() - 1));
		}

		randOre1Suffix = listSuffix.at(rand() % (listSuffix.size() - 1)); //We only have one prefix class, so no need for if statements (yet).

		randomOre1 = randOre1Prefix + randOre1Suffix;

		cout << randomOre1 << endl; //Instead of a cout, obviously, we'd assign the ore name to the value stored here.
		
	
		char exitLoop; // Beginning of debugging, so we can generate more random names if we want. 

		cout << "Enter 'Y' to generate another name, or enter anything else to terminate the program.\n";
		cin >> exitLoop;

		if (exitLoop == 'y' || exitLoop == 'Y') {
			continue;
		}
		else {
			break;
		}
		
		/* End of debugging.
		
		Now we would declare the attributes of the block, in the class file. 
		
		Since not all of these prefixes are going to be good, but they still affect the same attribute, the other file with the prefix and suffix dictionary should have these
		split up into good and bad lists like so:
		(I'm making up the block durability assignment, I don't know how the game handles it without looking)
		
		assuming we have two vectors, listPrefixDuraGood and listPrefixDuraBad...
		if (randomNumPrefix (the number that decided what prefix list to use, see line 89) == number that lead to use listPrefixDuraGood)
			blockdurability = blockdurability * 1.2;
		if (randomNumPrefix == number that lead to use listPrefixDuraBad)
			blockdurability = blockdurability * 0.8;
			
		
		
		or when it declares color:
			if (randOre1Prefix.find("Blu") != string::npos)   <-- This is probably a really shitty way of doing it considering just how many prefixes I'm going to add, but whatever.
				import color profile from the dictionary file??
				or just set it to like
				r = 30;
				g = 46;
				b = 212;
				
				
		then when the method to generate the ore in the world is called:
		when it's picking a max Y value (the amount of blocks high the ore can generate, 64 is sea level)
			if durability >= some high number,
				maxY = maxY - (durability * 1.2)
					This ensures the harder the ore, the deeper it'll be located.
					We can repeat the process similarly for things like VeinSize, which determines how big the ore "clusters" are.
		
				
		Again, not sure how Java works, nor do I know how Minecraft declares blocks and whatnot, but this should give you an idea of where my mind is at and form a good blueprint.
		*/
	}
	
	return 0;
}