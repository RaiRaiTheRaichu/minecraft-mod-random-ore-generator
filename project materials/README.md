TBA

### Links

Config file info, from forge modding wiki:
https://www.reddit.com/r/forgemodding/wiki/tutorials/config

Creative tabs, Recipes, and **Config** files:
https://youtu.be/iT-RMZP-acI

Texture coloring:
https://github.com/TeamMetallurgy/Metallurgy5/blob/master/src/main/java/com/teammetallurgy/m5/core/MetalResourceLoader.java
https://github.com/TeamMetallurgy/Metallurgy5/blob/master/src/main/java/com/teammetallurgy/m5/core/registry/HSLColor.java

Config dumping:
https://github.com/TeamMetallurgy/Metallurgy5/blob/master/src/main/java/com/teammetallurgy/m5/core/registry/MetalDefinition.java