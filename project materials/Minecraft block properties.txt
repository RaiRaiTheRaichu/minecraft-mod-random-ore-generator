Block properties:

1) Fall down when below one breaks / Levitates
2) Is destroyed when below one breaks / remains
3) Some need tools to get blocks from (i.e. Stone)
4) Tree blocks: SLowly disappear after the trunk is gone for a while
    a) Reasons:
       1) Not connected to the ground.


Misc. facts:

1) A name for the world is not enough to generate the same world; I believe a seed needs to be specified. Where to get this seed?