package com.OwO;

import net.minecraft.block.material.Material;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.ArrayList;
import java.util.List;

import com.OwO.block.BlockRandomOre;
import com.OwO.handlers.ConfigurationHandler;
import com.OwO.utils.ModConstants;
import com.OwO.worldgen.ModWorldGenerator;



//,,,,,........................................,,,,,,**,,********,,,,,,,,,,*#####
//,,,..........................................,,,,,,***,,,,,,,,**,*,,,,,,,,,,*##
//,............,/***,...........................,,,,*//*,,...,,,,,,**,,,,,,,,,,,#
//............,(((///*,.........................,,,*///*,.....,,,,,,,*,,,*****,,,
//............/(#(////**,........................,*////*,,.............,,,,,,,,,,
//...........,((((//////*,,.....................,*///(/**,..........,...........,
//.........../((((((((//((/*,...............,,**////////*,,,,,,,,,,,.,,,,........
//.........../((((#(#((//((//***********************,**//**///////*****,,,,,,,...
//.........../((((%%%%#(//(#(//(/*********************,,,,***///////////////*****
//...........*/(/(#%&%(////((#(*************/////****,,,,,,,,,***//////////////((
//............////(%%&(((((*******************//***,,,,,,,,,,,,,,**/*//////////
//............*////(#%&&&(//*********************//**,,,,,,,,,,,,,,,,****////////
//...........,*/(%///(((/**********//****,,,,***///***,,,,*,,,,,,,,,,,,*****/////
//..........,,*/((////**,*********////**,,,,,,,**////**,,*/%&&(,,,,,,,,,,******//
//.........,,*/(((///*,,**********/*******,*,,,**********/%(##(/,.. ..,,,,,,,,,,,
//.........,**/##//*****************//#@@&&/*****,,,*,,,**(&%##/,..   ..,,,,.,,,,
//.........,*//((/****************/(&@&&&%*/&%(//*,,,,,,,,,,/#/*,,..  ..,.,,,,,,,
//.........,***/**********,*,,,,,***(#&@&%(/&&(/**,,,,,,,,,,,,,,,,,.... .,,.,,,,,
//........,**********,,,,,,,,,,,,,,,,,/(((##/**/***,,,,,,,,,,,,.,,........,,,,,,,
//........,*******,,,,,,,,,*,,,,.,,,,*************,,,,,,,,***//((*........,,,,,,,
//.......,******,*,,,,,,,,,,,,.,,,,,**,****,,,*,,,,,.,,,,(%%%%&&&&&%......,,*,,,,
//.......,*******,,,,,,,,,,,,,,,,,,,,,,,.,,,,,,,,,,,,.,,/&&&@@@@&&@%,..,,..*****/
//......,******/**,*,,,,,,,,,,,,,,,,,,,...,,,,,,,,,,,,,,*%&@@&&&&&&&/,......,,***
//.....,,******/*******,**,,,,,,,,,,,,,,,,,,,,,*,,,,,*,/*/(%&&&&@&%#/,,..,.,,,*,,
//.....,,*****///***********,,,,,,,,,,,,,,,,,,,*******(**/(##%%%&%##(*,,....,***,
//....,,***/***//*****,*,,,,****,,**,*,,************/**///##%##%%%#((*,,...,,****
//....,,********/****,,,,,,,,,***,,,**********///******//#&@@@@&%%%#,...,..,,****
//...,,******/**//****,,,,,,,,,,,,*,,,,*******/(#%&&%%%&&%%&%%&%%#/*,,...........
//...,****///////*//****,,,,,,,,*,,,,,**********///((((##(((((((/**,,............
//..,,***///////**///*******,,,,,,*,,******************/***/*//*,,,,,,...........
//..,,*//*///////**///********,,,,,,,*,******************,,,,,,,,,,,,,,..........
//.,,******//////////*******,,***,,,,,,,*****************,,,,,,,,,,,,,,,.........
//.,,,******/////(///***********,,,*,,,,,,***************,,,**,,,,,,,,,,.........
//..,,,,******///////*********,***,*,,,**,****************,,,,,,,,,,,,,,.........
//..,,**,*******/////************,*************************,,,,,,*,,,,,,,........
//..,,,,,,,,,,,,,**//************,*****//***********************,,,,,,,,,......,.
//...,*,,,,,,,*,,*,,,,****,****,,,**,*****/*//////////*////***,,,,,,,,,.....,,,,,
//*.,,,,,,,,,,,,,,,,,,,,,,**,,,**,,**,,,,*****//////*/*********,,,,,,,.....,,,,,/
//****,,,,,,,,,,,,,,,,,,,,,,,,,,***,****,*,,********/*********,,,,,,,......,,,,//

/*******************************************************************************
 * <b>Class:</b> RandomOreMain
 *
 * <p>
 * This class is the entry point into the Random Ore Mod.
 ******************************************************************************/
@Mod(modid = ModConstants.MODID, name = ModConstants.NAME, version = ModConstants.VERSION)
public class RandomOreMain 
{
    //==========================================================================
    // Section: Fields
    //==========================================================================

    //--------------------------------------------------------------------------
    // Class (Static) Fields
    //--------------------------------------------------------------------------

    /**
     * A list of all of the randomly generated ores.
     */
    public static List<BlockRandomOre> randomOres = new ArrayList<BlockRandomOre>();

    //--------------------------------------------------------------------------
    // Instance Fields
    //--------------------------------------------------------------------------

    //==========================================================================
    // Section: Constructors
    //==========================================================================

    //==========================================================================
    // Section: Methods
    //==========================================================================

    //--------------------------------------------------------------------------
    // Public Methods
    //--------------------------------------------------------------------------

    @Mod.EventHandler // Difference between Mod.EventHandler and EventHandler?
    public static void preInit (FMLPreInitializationEvent event) {	// Configs are apparently done in the PreInit phase
    	String configDir = event.getModConfigurationDirectory().toString();	// Setting the configDir for this method
    	ConfigurationHandler.init(configDir); // Initialize the directory
    	FMLCommonHandler.instance().bus().register(new ConfigurationHandler()); // Registers the configuration handler
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        for(int i = 0; i < 1; i++) // Register 1 ore, once we are able to successfully generate more than one ore, the "i < 1" should be a value read from config, possibly with a cap
        {
            // A default name for our ore, for now....
            String oreName = "randomOre";

            // Registering a new block with the name stored in oreName and of the material "rock".
            // Note, as Sean stated, oreName will probably end up being "oreName" + i, or something,
            // to differentiate them. IF THE LOOP IS INCREASED IN SIZE, THIS WILL BE REQUIRED, and
            // other changes (such as to en_US.lang, as well as the naming conventions for our texture
            // files) will need to be setup.
            BlockRandomOre randomOre = new BlockRandomOre(Material.rock, oreName);
            GameRegistry.registerBlock(randomOre, oreName);
            GameRegistry.registerWorldGenerator(new ModWorldGenerator(randomOre, 6), 0); // 6 is the vein size

            // Store the ore, for later. If we need to be able to find an ore by name or something later on,
            // we'll need to devise a way to easily get ores back (by name, by number, etc.).
            randomOres.add(randomOre);
	    }
    }
    
    

    //--------------------------------------------------------------------------
    // Package Methods
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Internal Methods
    //--------------------------------------------------------------------------
}