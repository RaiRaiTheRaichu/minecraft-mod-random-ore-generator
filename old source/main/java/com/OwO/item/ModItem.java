package com.OwO.item;

//,,,,,........................................,,,,,,**,,********,,,,,,,,,,*#####
//,,,..........................................,,,,,,***,,,,,,,,**,*,,,,,,,,,,*##
//,............,/***,...........................,,,,*//*,,...,,,,,,**,,,,,,,,,,,#
//............,(((///*,.........................,,,*///*,.....,,,,,,,*,,,*****,,,
//............/(#(////**,........................,*////*,,.............,,,,,,,,,,
//...........,((((//////*,,.....................,*///(/**,..........,...........,
//.........../((((((((//((/*,...............,,**////////*,,,,,,,,,,,.,,,,........
//.........../((((#(#((//((//***********************,**//**///////*****,,,,,,,...
//.........../((((%%%%#(//(#(//(/*********************,,,,***///////////////*****
//...........*/(/(#%&%(////((#(*************/////****,,,,,,,,,***//////////////((
//............////(%%&(((((*******************//***,,,,,,,,,,,,,,**/*//////////
//............*////(#%&&&(//*********************//**,,,,,,,,,,,,,,,,****////////
//...........,*/(%///(((/**********//****,,,,***///***,,,,*,,,,,,,,,,,,*****/////
//..........,,*/((////**,*********////**,,,,,,,**////**,,*/%&&(,,,,,,,,,,******//
//.........,,*/(((///*,,**********/*******,*,,,**********/%(##(/,.. ..,,,,,,,,,,,
//.........,**/##//*****************//#@@&&/*****,,,*,,,**(&%##/,..   ..,,,,.,,,,
//.........,*//((/****************/(&@&&&%*/&%(//*,,,,,,,,,,/#/*,,..  ..,.,,,,,,,
//.........,***/**********,*,,,,,***(#&@&%(/&&(/**,,,,,,,,,,,,,,,,,.... .,,.,,,,,
//........,**********,,,,,,,,,,,,,,,,,/(((##/**/***,,,,,,,,,,,,.,,........,,,,,,,
//........,*******,,,,,,,,,*,,,,.,,,,*************,,,,,,,,***//((*........,,,,,,,
//.......,******,*,,,,,,,,,,,,.,,,,,**,****,,,*,,,,,.,,,,(%%%%&&&&&%......,,*,,,,
//.......,*******,,,,,,,,,,,,,,,,,,,,,,,.,,,,,,,,,,,,.,,/&&&@@@@&&@%,..,,..*****/
//......,******/**,*,,,,,,,,,,,,,,,,,,,...,,,,,,,,,,,,,,*%&@@&&&&&&&/,......,,***
//.....,,******/*******,**,,,,,,,,,,,,,,,,,,,,,*,,,,,*,/*/(%&&&&@&%#/,,..,.,,,*,,
//.....,,*****///***********,,,,,,,,,,,,,,,,,,,*******(**/(##%%%&%##(*,,....,***,
//....,,***/***//*****,*,,,,****,,**,*,,************/**///##%##%%%#((*,,...,,****
//....,,********/****,,,,,,,,,***,,,**********///******//#&@@@@&%%%#,...,..,,****
//...,,******/**//****,,,,,,,,,,,,*,,,,*******/(#%&&%%%&&%%&%%&%%#/*,,...........
//...,****///////*//****,,,,,,,,*,,,,,**********///((((##(((((((/**,,............
//..,,***///////**///*******,,,,,,*,,******************/***/*//*,,,,,,...........
//..,,*//*///////**///********,,,,,,,*,******************,,,,,,,,,,,,,,..........
//.,,******//////////*******,,***,,,,,,,*****************,,,,,,,,,,,,,,,.........
//.,,,******/////(///***********,,,*,,,,,,***************,,,**,,,,,,,,,,.........
//..,,,,******///////*********,***,*,,,**,****************,,,,,,,,,,,,,,.........
//..,,**,*******/////************,*************************,,,,,,*,,,,,,,........
//..,,,,,,,,,,,,,**//************,*****//***********************,,,,,,,,,......,.
//...,*,,,,,,,*,,*,,,,****,****,,,**,*****/*//////////*////***,,,,,,,,,.....,,,,,
//*.,,,,,,,,,,,,,,,,,,,,,,**,,,**,,**,,,,*****//////*/*********,,,,,,,.....,,,,,/
//****,,,,,,,,,,,,,,,,,,,,,,,,,,***,****,*,,********/*********,,,,,,,......,,,,//

/*******************************************************************************
 * <b>Class:</b> ModItem
 *
 * <p>
 * Represents an item in the Random Ore Mod.
 ******************************************************************************/
public class ModItem // should probably extend Item, but not needed (I don't think?)
{
    // THIS IS ALL PLACEHOLDER FOR FUTURE USE

    //==========================================================================
    // Section: Fields
    //==========================================================================

    //--------------------------------------------------------------------------
    // Class (Static) Fields
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance Fields
    //--------------------------------------------------------------------------

    //==========================================================================
    // Section: Constructors
    //==========================================================================

    //==========================================================================
    // Section: Methods
    //==========================================================================

    //--------------------------------------------------------------------------
    // Public Methods
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Package Methods
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Internal Methods
    //--------------------------------------------------------------------------
}
