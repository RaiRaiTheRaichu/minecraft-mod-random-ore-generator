package com.OwO;

import java.util.ArrayList;
import java.util.Random;

//,,,,,........................................,,,,,,**,,********,,,,,,,,,,*#####
//,,,..........................................,,,,,,***,,,,,,,,**,*,,,,,,,,,,*##
//,............,/***,...........................,,,,*//*,,...,,,,,,**,,,,,,,,,,,#
//............,(((///*,.........................,,,*///*,.....,,,,,,,*,,,*****,,,
//............/(#(////**,........................,*////*,,.............,,,,,,,,,,
//...........,((((//////*,,.....................,*///(/**,..........,...........,
//.........../((((((((//((/*,...............,,**////////*,,,,,,,,,,,.,,,,........
//.........../((((#(#((//((//***********************,**//**///////*****,,,,,,,...
//.........../((((%%%%#(//(#(//(/*********************,,,,***///////////////*****
//...........*/(/(#%&%(////((#(*************/////****,,,,,,,,,***//////////////((
//............////(%%&(((((*******************//***,,,,,,,,,,,,,,**/*//////////
//............*////(#%&&&(//*********************//**,,,,,,,,,,,,,,,,****////////
//...........,*/(%///(((/**********//****,,,,***///***,,,,*,,,,,,,,,,,,*****/////
//..........,,*/((////**,*********////**,,,,,,,**////**,,*/%&&(,,,,,,,,,,******//
//.........,,*/(((///*,,**********/*******,*,,,**********/%(##(/,.. ..,,,,,,,,,,,
//.........,**/##//*****************//#@@&&/*****,,,*,,,**(&%##/,..   ..,,,,.,,,,
//.........,*//((/****************/(&@&&&%*/&%(//*,,,,,,,,,,/#/*,,..  ..,.,,,,,,,
//.........,***/**********,*,,,,,***(#&@&%(/&&(/**,,,,,,,,,,,,,,,,,.... .,,.,,,,,
//........,**********,,,,,,,,,,,,,,,,,/(((##/**/***,,,,,,,,,,,,.,,........,,,,,,,
//........,*******,,,,,,,,,*,,,,.,,,,*************,,,,,,,,***//((*........,,,,,,,
//.......,******,*,,,,,,,,,,,,.,,,,,**,****,,,*,,,,,.,,,,(%%%%&&&&&%......,,*,,,,
//.......,*******,,,,,,,,,,,,,,,,,,,,,,,.,,,,,,,,,,,,.,,/&&&@@@@&&@%,..,,..*****/
//......,******/**,*,,,,,,,,,,,,,,,,,,,...,,,,,,,,,,,,,,*%&@@&&&&&&&/,......,,***
//.....,,******/*******,**,,,,,,,,,,,,,,,,,,,,,*,,,,,*,/*/(%&&&&@&%#/,,..,.,,,*,,
//.....,,*****///***********,,,,,,,,,,,,,,,,,,,*******(**/(##%%%&%##(*,,....,***,
//....,,***/***//*****,*,,,,****,,**,*,,************/**///##%##%%%#((*,,...,,****
//....,,********/****,,,,,,,,,***,,,**********///******//#&@@@@&%%%#,...,..,,****
//...,,******/**//****,,,,,,,,,,,,*,,,,*******/(#%&&%%%&&%%&%%&%%#/*,,...........
//...,****///////*//****,,,,,,,,*,,,,,**********///((((##(((((((/**,,............
//..,,***///////**///*******,,,,,,*,,******************/***/*//*,,,,,,...........
//..,,*//*///////**///********,,,,,,,*,******************,,,,,,,,,,,,,,..........
//.,,******//////////*******,,***,,,,,,,*****************,,,,,,,,,,,,,,,.........
//.,,,******/////(///***********,,,*,,,,,,***************,,,**,,,,,,,,,,.........
//..,,,,******///////*********,***,*,,,**,****************,,,,,,,,,,,,,,.........
//..,,**,*******/////************,*************************,,,,,,*,,,,,,,........
//..,,,,,,,,,,,,,**//************,*****//***********************,,,,,,,,,......,.
//...,*,,,,,,,*,,*,,,,****,****,,,**,*****/*//////////*////***,,,,,,,,,.....,,,,,
//*.,,,,,,,,,,,,,,,,,,,,,,**,,,**,,**,,,,*****//////*/*********,,,,,,,.....,,,,,/
//****,,,,,,,,,,,,,,,,,,,,,,,,,,***,****,*,,********/*********,,,,,,,......,,,,//

/*******************************************************************************
 * <b>Class:</b> RandomList
 *
 * <p>
 * A <code>RandomList</code> represents a list of objects that can be accessed
 * in a random fashion.
 * <p>
 * A RandomList allows a user to get its components in two different modes,
 * both of which are random:
 * <ol>
 * <li>Allowing repeats</li>
 * <li>Not allowing repeats until all options have been chosen</li>
 * </ol>
 * To help illustrate how this works, consider a hat with names on pieces of
 * paper in it. The two situations are explained as such.
 * <ol>
 * <li>Mode 1: You pull a name out of the hat, read the name, and put
 * the piece of paper back into the hat. Next time you pull a name out, you may
 * get the same name, or (more likely) you'll get a different name out.
 * </li>
 * <li>Mode 2: You pull a name out of the hat, read the name, and
 * then put the piece of paper off to the side. Next time you pull a name out,
 * you're guaranteed you won't pull the same name out. The process continues
 * until there are no more names in the hat. At that point, all the names are
 * put back into the hat, and the process can begin again.
 * </li>
 * </ol>
 * <p>
 * A user of this class can switch between the modes. Note though that when
 * switching back into Mode 2, you will start off with a fresh slate.
 ******************************************************************************/
public class RandomList<T extends Cloneable>
{
    //==========================================================================
    // Section: Fields
    //==========================================================================

    //--------------------------------------------------------------------------
    // Class (Static) Fields
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance Fields
    //--------------------------------------------------------------------------
    
    // Members for both Modes
    
    /**
     * This is the master list of objects that can be returned for requests.
     */
    private ArrayList<T> masterList;
    
    /**
     * This value dictates whether we are operating in Mode 1 (true) or
     * Mode 2 (false)
     */
    private boolean wantRepeats;
    
    /**
     * This is the {@link Random} object that we will utilize for random
     * indices to return the next random object.
     */
    private Random random;
    
    // Members for Mode 2
    
    /**
     * This is the list that we will be getting objects from when in Mode 2.
     * This list starts off identical to <code>masterList</code> and will
     * be decremented as we request random objects. Once this list is empty,
     * we re-stock it with <code>masterList</code>
     */
    private ArrayList<T> mode2List;
    

    //==========================================================================
    // Section: Constructors
    //==========================================================================
    
    /**
     * Basic constructor for this class. The second argument dictates whether
     * we will run in Mode 1 (true) or Mode 2 (false).
     */
    public RandomList(Random random, boolean wantRepeats)
    {
        // Empty lists for now
        masterList = new ArrayList<T>();
        this.mode2List = new ArrayList<T>();
        
        this.random = random;
        this.wantRepeats = wantRepeats;
    }

    //==========================================================================
    // Section: Methods
    //==========================================================================

    //--------------------------------------------------------------------------
    // Public Methods
    //--------------------------------------------------------------------------
    
    /**
     * Adds an object to the list.
     * 
     * @param object The object to add to the list
     */
    public void add(T object)
    {
        masterList.add(object);
        
        if(isInMode2())
        {
            mode2List.add(object);
        }
    }
    
    /**
     * Clears the list of all objects.
     */
    public void clear()
    {
        masterList.clear();
        mode2List.clear();
    }
    
    /**
     * Returns a random object from the list.
     */
    public T getObject() throws IndexOutOfBoundsException
    {
        // Make sure the masterList is not empty.
        if(masterList.isEmpty())
            throw new IndexOutOfBoundsException ("Empty Random List");
        
        // Get a random index.
        int bound = isInMode1() ? masterList.size() : mode2List.size();
        int randomIndex = random.nextInt(bound);
        
        // Get an object from the appropriate list
        return isInMode1() ? masterList.get(randomIndex) : mode2List.remove(randomIndex);
    }
    
    /**
     * This method switches from Mode 1 to Mode 2, and vice-versa.
     */
    public void switchModes()
    {
        wantRepeats = !wantRepeats; 
        
        if(isInMode2())
            initMode2List();
    }
    
    /**
     * Returns whether we are running in Mode 1 or not.
     * 
     * @return true if we are running in Mode 1
     */
    public boolean isInMode1()
    {
        return wantRepeats;
    }
    
    /**
     * Returns whether we are running in Mode 2 or not.
     * 
     * @return  true if we are running in Mode 2
     */
    public boolean isInMode2()
    {
        return !wantRepeats;
    }

    //--------------------------------------------------------------------------
    // Package Methods
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Internal Methods
    //--------------------------------------------------------------------------
    
    private void initMode2List()
    {
        // Need to write a method. Do we need to clear, and then copy masterList?
        // Research how often we need to clear, and what would be the most efficient
        // way to do this. Maybe null the list until we're ready? IDK.
    }
}
