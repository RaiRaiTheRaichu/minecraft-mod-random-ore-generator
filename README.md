# Randomized Ore Generator - Minecraft Forge mod

Most information here is currently placeholder text.

## Getting Started

TBA

Link: https://techwiseacademy.com/minecraft-modding-setting-up-your-environment/


## Prerequisites

Project is being built upon the following frameworks/APIs/languages:

```
x64 JDK 1.8.0_191 
Forge Mod Loader (FML) version 1.7.10 - 10.13.4.1614
FasterXML's Jackson version 2.11.3 - https://github.com/FasterXML/jackson
```

## Project Roadmap

```
More info: See README.md in project materials folder for links.
```

### Framework:

-Setting up ore template, verifying custom ores can spawn correctly
	
-Allow attributes of the ore to be dumped to a config file (Our config file will be added to the /config/ folder when the game is launched in our test environment)

-Allow changes in the config file to ore attributes to recursively(?) change the attributes when they are loaded on the start of the game

-Build prefix-suffix dictionaries, complete with groups of similar ones

### Phase 2:

-Roughly implement randomly-generated display names for ores

-Implement per-world-seed configuration (Use world seed for the seed when generating random attributes for the ore)

### TBA

Textures, carry over the name of ore to the ingot that can be smelted from the ore, carry over names of ores to the tools that can be crafted, allow advanced attributes such as "isFuel" and different types of minerals besides ores (fuels, crystals, etc) and have the item dropped upon harvesting reflect these attribute flags

## Internal Info

```
Package name: com.OwO
Main java class: com.OwO.RandomOreMain.java
MODID: RandomOre (NOTE: This is inconsistent with the main java class which, in tutorials, was simply named Main.java. The MODID "RandomOre" is very similar to the main class file "RandomOreMain", so that needs to be sorted out ASAP.
```

## Installing

TBA...

## Setting up your Development Environment (Eclipse)

```
Note: Starting with Eclipse 2020-09, Java 11 is needed to
run Eclipse. To be able to follow the instructions below:

1) Make sure that eclipse.ini has the "-vm" argument pointing to a valid Java 11 JDK.
   * See Eclipse documentation about this argument.
2) Make sure to update the project-specific JRE/JDK to be a local Java 8 one after importing your project below.
   * Right-click project -> Build Path -> Configure Build Path...
   -> Libraries -> Highlight 'JRE System Library' -> Edit...
```

1: Checkout the codebase.

2: Ensure you have Java 8 (preferably 1.8.0_191 or newer) as your JAVA_HOME.

3: Run 'gradlew setupDecompWorkspace' at the root of your checkout.

4: Run 'gradlew eclipse' at the root of your checkout.

5: Import the Gradle project into Eclipse.

6) To test changes after making them, either:
    a) Run 'gradlew runclient' at the root of your checkout
    b) Use the 'minecraft-mod-random-ore-generator_Client' Run configuration. (Though not sure, this doesn't work with newer Eclipses it looks like.)

RECOMMENDED ECLIPSE PLUGINS

1) SonarLint (Eclipse Marketplace under 'Help', continuously analyzes the code you write for good practices)

## Versioning

Version 0.1 (starting off...)


