package com.OwO.RandomOre.config;

import com.OwO.RandomOre.utils.ModConstants;

import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.common.config.Config.Type;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.client.event.ConfigChangedEvent.OnConfigChangedEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

// ,,,,,........................................,,,,,,**,,********,,,,,,,,,,*#####
// ,,,..........................................,,,,,,***,,,,,,,,**,*,,,,,,,,,,*##
// ,............,/***,...........................,,,,*//*,,...,,,,,,**,,,,,,,,,,,#
// ............,(((///*,.........................,,,*///*,.....,,,,,,,*,,,*****,,,
// ............/(#(////**,........................,*////*,,.............,,,,,,,,,,
// ...........,((((//////*,,.....................,*///(/**,..........,...........,
// .........../((((((((//((/*,...............,,**////////*,,,,,,,,,,,.,,,,........
// .........../((((#(#((//((//***********************,**//**///////*****,,,,,,,...
// .........../((((%%%%#(//(#(//(/*********************,,,,***///////////////*****
// ...........*/(/(#%&%(////((#(*************/////****,,,,,,,,,***//////////////((
// ............////(%%&(((((*******************//***,,,,,,,,,,,,,,**/*//////////
// ............*////(#%&&&(//*********************//**,,,,,,,,,,,,,,,,****////////
// ...........,*/(%///(((/**********//****,,,,***///***,,,,*,,,,,,,,,,,,*****/////
// ..........,,*/((////**,*********////**,,,,,,,**////**,,*/%&&(,,,,,,,,,,******//
// .........,,*/(((///*,,**********/*******,*,,,**********/%(##(/,.. ..,,,,,,,,,,,
// .........,**/##//*****************//#@@&&/*****,,,*,,,**(&%##/,..   ..,,,,.,,,,
// .........,*//((/****************/(&@&&&%*/&%(//*,,,,,,,,,,/#/*,,..  ..,.,,,,,,,
// .........,***/**********,*,,,,,***(#&@&%(/&&(/**,,,,,,,,,,,,,,,,,.... .,,.,,,,,
// ........,**********,,,,,,,,,,,,,,,,,/(((##/**/***,,,,,,,,,,,,.,,........,,,,,,,
// ........,*******,,,,,,,,,*,,,,.,,,,*************,,,,,,,,***//((*........,,,,,,,
// .......,******,*,,,,,,,,,,,,.,,,,,**,****,,,*,,,,,.,,,,(%%%%&&&&&%......,,*,,,,
// .......,*******,,,,,,,,,,,,,,,,,,,,,,,.,,,,,,,,,,,,.,,/&&&@@@@&&@%,..,,..*****/
// ......,******/**,*,,,,,,,,,,,,,,,,,,,...,,,,,,,,,,,,,,*%&@@&&&&&&&/,......,,***
// .....,,******/*******,**,,,,,,,,,,,,,,,,,,,,,*,,,,,*,/*/(%&&&&@&%#/,,..,.,,,*,,
// .....,,*****///***********,,,,,,,,,,,,,,,,,,,*******(**/(##%%%&%##(*,,....,***,
// ....,,***/***//*****,*,,,,****,,**,*,,************/**///##%##%%%#((*,,...,,****
// ....,,********/****,,,,,,,,,***,,,**********///******//#&@@@@&%%%#,...,..,,****
// ...,,******/**//****,,,,,,,,,,,,*,,,,*******/(#%&&%%%&&%%&%%&%%#/*,,...........
// ...,****///////*//****,,,,,,,,*,,,,,**********///((((##(((((((/**,,............
// ..,,***///////**///*******,,,,,,*,,******************/***/*//*,,,,,,...........
// ..,,*//*///////**///********,,,,,,,*,******************,,,,,,,,,,,,,,..........
// .,,******//////////*******,,***,,,,,,,*****************,,,,,,,,,,,,,,,.........
// .,,,******/////(///***********,,,*,,,,,,***************,,,**,,,,,,,,,,.........
// ..,,,,******///////*********,***,*,,,**,****************,,,,,,,,,,,,,,.........
// ..,,**,*******/////************,*************************,,,,,,*,,,,,,,........
// ..,,,,,,,,,,,,,**//************,*****//***********************,,,,,,,,,......,.
// ...,*,,,,,,,*,,*,,,,****,****,,,**,*****/*//////////*////***,,,,,,,,,.....,,,,,
// *.,,,,,,,,,,,,,,,,,,,,,,**,,,**,,**,,,,*****//////*/*********,,,,,,,.....,,,,,/
// ****,,,,,,,,,,,,,,,,,,,,,,,,,,***,****,*,,********/*********,,,,,,,......,,,,//

/*******************************************************************************
 * Syncs up all changes to config files from the Config GUI per the Javadoc in
 * {@link OnConfigChangedEvent}.
 * <br><br>
 * <i>(Really, this is boilerplate code and Forge should be syncing the config
 * itself...)</i>
 ******************************************************************************/
@EventBusSubscriber
public class ConfigChangeHandler {
    @SubscribeEvent
    public static void onConfigChanged(final ConfigChangedEvent.OnConfigChangedEvent event) {
        if (event.getModID().equals(ModConstants.MODID)) {
            ConfigManager.sync(ModConstants.MODID, Type.INSTANCE);
        }
    }
}
