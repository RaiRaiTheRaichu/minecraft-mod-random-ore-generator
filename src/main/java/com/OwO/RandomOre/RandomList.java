package com.OwO.RandomOre;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;

// ,,,,,........................................,,,,,,**,,********,,,,,,,,,,*#####
// ,,,..........................................,,,,,,***,,,,,,,,**,*,,,,,,,,,,*##
// ,............,/***,...........................,,,,*//*,,...,,,,,,**,,,,,,,,,,,#
// ............,(((///*,.........................,,,*///*,.....,,,,,,,*,,,*****,,,
// ............/(#(////**,........................,*////*,,.............,,,,,,,,,,
// ...........,((((//////*,,.....................,*///(/**,..........,...........,
// .........../((((((((//((/*,...............,,**////////*,,,,,,,,,,,.,,,,........
// .........../((((#(#((//((//***********************,**//**///////*****,,,,,,,...
// .........../((((%%%%#(//(#(//(/*********************,,,,***///////////////*****
// ...........*/(/(#%&%(////((#(*************/////****,,,,,,,,,***//////////////((
// ............////(%%&(((((*******************//***,,,,,,,,,,,,,,**/*//////////
// ............*////(#%&&&(//*********************//**,,,,,,,,,,,,,,,,****////////
// ...........,*/(%///(((/**********//****,,,,***///***,,,,*,,,,,,,,,,,,*****/////
// ..........,,*/((////**,*********////**,,,,,,,**////**,,*/%&&(,,,,,,,,,,******//
// .........,,*/(((///*,,**********/*******,*,,,**********/%(##(/,.. ..,,,,,,,,,,,
// .........,**/##//*****************//#@@&&/*****,,,*,,,**(&%##/,..   ..,,,,.,,,,
// .........,*//((/****************/(&@&&&%*/&%(//*,,,,,,,,,,/#/*,,..  ..,.,,,,,,,
// .........,***/**********,*,,,,,***(#&@&%(/&&(/**,,,,,,,,,,,,,,,,,.... .,,.,,,,,
// ........,**********,,,,,,,,,,,,,,,,,/(((##/**/***,,,,,,,,,,,,.,,........,,,,,,,
// ........,*******,,,,,,,,,*,,,,.,,,,*************,,,,,,,,***//((*........,,,,,,,
// .......,******,*,,,,,,,,,,,,.,,,,,**,****,,,*,,,,,.,,,,(%%%%&&&&&%......,,*,,,,
// .......,*******,,,,,,,,,,,,,,,,,,,,,,,.,,,,,,,,,,,,.,,/&&&@@@@&&@%,..,,..*****/
// ......,******/**,*,,,,,,,,,,,,,,,,,,,...,,,,,,,,,,,,,,*%&@@&&&&&&&/,......,,***
// .....,,******/*******,**,,,,,,,,,,,,,,,,,,,,,*,,,,,*,/*/(%&&&&@&%#/,,..,.,,,*,,
// .....,,*****///***********,,,,,,,,,,,,,,,,,,,*******(**/(##%%%&%##(*,,....,***,
// ....,,***/***//*****,*,,,,****,,**,*,,************/**///##%##%%%#((*,,...,,****
// ....,,********/****,,,,,,,,,***,,,**********///******//#&@@@@&%%%#,...,..,,****
// ...,,******/**//****,,,,,,,,,,,,*,,,,*******/(#%&&%%%&&%%&%%&%%#/*,,...........
// ...,****///////*//****,,,,,,,,*,,,,,**********///((((##(((((((/**,,............
// ..,,***///////**///*******,,,,,,*,,******************/***/*//*,,,,,,...........
// ..,,*//*///////**///********,,,,,,,*,******************,,,,,,,,,,,,,,..........
// .,,******//////////*******,,***,,,,,,,*****************,,,,,,,,,,,,,,,.........
// .,,,******/////(///***********,,,*,,,,,,***************,,,**,,,,,,,,,,.........
// ..,,,,******///////*********,***,*,,,**,****************,,,,,,,,,,,,,,.........
// ..,,**,*******/////************,*************************,,,,,,*,,,,,,,........
// ..,,,,,,,,,,,,,**//************,*****//***********************,,,,,,,,,......,.
// ...,*,,,,,,,*,,*,,,,****,****,,,**,*****/*//////////*////***,,,,,,,,,.....,,,,,
// *.,,,,,,,,,,,,,,,,,,,,,,**,,,**,,**,,,,*****//////*/*********,,,,,,,.....,,,,,/
// ****,,,,,,,,,,,,,,,,,,,,,,,,,,***,****,*,,********/*********,,,,,,,......,,,,//

/*******************************************************************************
 * A <code>RandomList</code> represents a list of elements that can be accessed
 * in a random fashion.
 * <p>
 * <code>RandomList</code>s can be created from any {@link Collection Java
 * Collection}, meaning that they allow duplicate objects in them (including
 * the same object more than once).
 * <p>
 * At construction time, a <code>RandomList</code> can be configured to return
 * elements in one of two ways:
 * <br><br>
 * <strong><u>Allowing Repeats</u></strong>
 * <br><br>
 * If the supplying Collection had, for example, 20 elements in it, each
 * request can return any of the 20 elements.
 * <br><br>
 * <strong><u>Exhausting All Choices Before Allowing Repeats</u></strong>
 * <br><br>
 * If the supplying Collection had, for example, 20 elements in it, the first
 * request can return any of the 20 elements, the second request can return
 * any of the remaining 19 elements not already chosen, and so on. Once the list
 * has been exhausted, all choices are available again.
 * <p>
 * <strong>As is evident from the above documentation, "repeating" doesn't
 * care whether two objects are duplicates of each other or the same
 * object in any way for its definition.</strong>
 ******************************************************************************/
public class RandomList<T> {
    /**
     * The list of all possible elements that can be returned for requests.
     * This list never changes.
     */
    private List<T> masterList;

    /**
     * The current list of elements that can be returned for requests when
     * repeats are not wanted. The number of elements in this list changes
     * as this RandomList is accessed.
     */
    private List<T> noRepeatList;

    /**
     * Dictates whether repeats are allowed or not.
     */
    private boolean wantRepeats;

    /**
     * Pseudorandom number generator for index determination.
     */
    private Random random;


    /**
     * Creates a new list from the specified collection.
     *
     * @param collection a non-null, non-empty Collection
     * @param wantRepeats true if repeats are allowed, false if all choices
     *        should be exhausted before allowing repeats.
     */
    public RandomList(Collection<T> collection, boolean wantRepeats) {
        init(new Random(), collection, wantRepeats);
    }

    /**
     * Creates a new list from the specified collection, allowing the
     * specification of a seed value for reproducibility purposes.
     *
     * @param seed seed value that controls the randomness of element retrieval
     * @param collection a non-null, non-empty Collection
     * @param wantRepeats true if repeats are allowed, false if all choices
     *        should be exhausted before allowing repeats.
     */
    public RandomList(long seed, Collection<T> collection, boolean wantRepeats) {
        init(new Random(seed), collection, wantRepeats);
    }


    /**
     * Helper method to the constructors.
     */
    private void init(Random random, Collection<T> collection, boolean wantRepeats) {
        if (Objects.requireNonNull(collection).isEmpty()) {
            throw new IllegalArgumentException("collection can't be empty");
        }

        this.random = Objects.requireNonNull(random);
        masterList = Collections.unmodifiableList(new ArrayList<>(collection));
        this.wantRepeats = wantRepeats;

        if (!allowsRepeats()) {
            noRepeatList = new ArrayList<>();
        }
    }

    /**
     * Returns true if this list allows repeats, false if repeats aren't
     * allowed until all choices are exhausted.
     *
     * @return see above
     */
    public boolean allowsRepeats() {
        return wantRepeats;
    }

    /**
     * Returns a random element from the list based on its configuration.
     *
     * @return see above
     */
    public T getElement() {
        if (allowsRepeats()) {
            return masterList.get(random.nextInt(masterList.size()));
        }
        else {
            if (noRepeatList.isEmpty()) {
                //Replenish the list.
                noRepeatList.addAll(masterList);
            }

            return noRepeatList.remove(random.nextInt(noRepeatList.size()));
        }
    }

    /**
     * Returns a List of all elements available as choices. Note that while
     * the supplying Collection was not necessarily a List, this will always
     * give back one. Modification of the returned list doesn't impact this
     * RandomList, though mutable elements from it can still be modified.
     *
     * @return see above
     */
    public List<T> getElements() {
        return new ArrayList<>(masterList);
    }
}
