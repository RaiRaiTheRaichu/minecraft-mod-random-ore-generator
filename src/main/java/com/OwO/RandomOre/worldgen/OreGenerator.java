package com.OwO.RandomOre.worldgen;

import java.util.Random;

import com.OwO.RandomOre.block.init.BlockInitializer;
import com.google.common.base.Predicate;

import net.minecraft.block.state.IBlockState;
import net.minecraft.block.state.pattern.BlockMatcher;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;

// ,,,,,........................................,,,,,,**,,********,,,,,,,,,,*#####
// ,,,..........................................,,,,,,***,,,,,,,,**,*,,,,,,,,,,*##
// ,............,/***,...........................,,,,*//*,,...,,,,,,**,,,,,,,,,,,#
// ............,(((///*,.........................,,,*///*,.....,,,,,,,*,,,*****,,,
// ............/(#(////**,........................,*////*,,.............,,,,,,,,,,
// ...........,((((//////*,,.....................,*///(/**,..........,...........,
// .........../((((((((//((/*,...............,,**////////*,,,,,,,,,,,.,,,,........
// .........../((((#(#((//((//***********************,**//**///////*****,,,,,,,...
// .........../((((%%%%#(//(#(//(/*********************,,,,***///////////////*****
// ...........*/(/(#%&%(////((#(*************/////****,,,,,,,,,***//////////////((
// ............////(%%&(((((*******************//***,,,,,,,,,,,,,,**/*//////////
// ............*////(#%&&&(//*********************//**,,,,,,,,,,,,,,,,****////////
// ...........,*/(%///(((/**********//****,,,,***///***,,,,*,,,,,,,,,,,,*****/////
// ..........,,*/((////**,*********////**,,,,,,,**////**,,*/%&&(,,,,,,,,,,******//
// .........,,*/(((///*,,**********/*******,*,,,**********/%(##(/,.. ..,,,,,,,,,,,
// .........,**/##//*****************//#@@&&/*****,,,*,,,**(&%##/,..   ..,,,,.,,,,
// .........,*//((/****************/(&@&&&%*/&%(//*,,,,,,,,,,/#/*,,..  ..,.,,,,,,,
// .........,***/**********,*,,,,,***(#&@&%(/&&(/**,,,,,,,,,,,,,,,,,.... .,,.,,,,,
// ........,**********,,,,,,,,,,,,,,,,,/(((##/**/***,,,,,,,,,,,,.,,........,,,,,,,
// ........,*******,,,,,,,,,*,,,,.,,,,*************,,,,,,,,***//((*........,,,,,,,
// .......,******,*,,,,,,,,,,,,.,,,,,**,****,,,*,,,,,.,,,,(%%%%&&&&&%......,,*,,,,
// .......,*******,,,,,,,,,,,,,,,,,,,,,,,.,,,,,,,,,,,,.,,/&&&@@@@&&@%,..,,..*****/
// ......,******/**,*,,,,,,,,,,,,,,,,,,,...,,,,,,,,,,,,,,*%&@@&&&&&&&/,......,,***
// .....,,******/*******,**,,,,,,,,,,,,,,,,,,,,,*,,,,,*,/*/(%&&&&@&%#/,,..,.,,,*,,
// .....,,*****///***********,,,,,,,,,,,,,,,,,,,*******(**/(##%%%&%##(*,,....,***,
// ....,,***/***//*****,*,,,,****,,**,*,,************/**///##%##%%%#((*,,...,,****
// ....,,********/****,,,,,,,,,***,,,**********///******//#&@@@@&%%%#,...,..,,****
// ...,,******/**//****,,,,,,,,,,,,*,,,,*******/(#%&&%%%&&%%&%%&%%#/*,,...........
// ...,****///////*//****,,,,,,,,*,,,,,**********///((((##(((((((/**,,............
// ..,,***///////**///*******,,,,,,*,,******************/***/*//*,,,,,,...........
// ..,,*//*///////**///********,,,,,,,*,******************,,,,,,,,,,,,,,..........
// .,,******//////////*******,,***,,,,,,,*****************,,,,,,,,,,,,,,,.........
// .,,,******/////(///***********,,,*,,,,,,***************,,,**,,,,,,,,,,.........
// ..,,,,******///////*********,***,*,,,**,****************,,,,,,,,,,,,,,.........
// ..,,**,*******/////************,*************************,,,,,,*,,,,,,,........
// ..,,,,,,,,,,,,,**//************,*****//***********************,,,,,,,,,......,.
// ...,*,,,,,,,*,,*,,,,****,****,,,**,*****/*//////////*////***,,,,,,,,,.....,,,,,
// *.,,,,,,,,,,,,,,,,,,,,,,**,,,**,,**,,,,*****//////*/*********,,,,,,,.....,,,,,/
// ****,,,,,,,,,,,,,,,,,,,,,,,,,,***,****,*,,********/*********,,,,,,,......,,,,//

/*******************************************************************************
 * [Insert_Description_Here]
 ******************************************************************************/
public class OreGenerator implements IWorldGenerator {
    /*
     * TODO remove when possible.
    enum WorldDimension {
      NETHER(-1),
      OVERWORLD(0),
      END(1);

      private int id;


      private WorldDimension(int id) {
        this.id = id;
      }


      public int value() {
        return id;
      }
    }
    */

    private static final int BLOCK_AMOUNT = 7;
    private static final int CHANCES_TO_SPAWN = 10;
    private static final int MIN_HEIGHT = 12;
    private static final int MAX_HEIGHT = 50;


    @Override
    public void generate(Random random, int chunkX, int chunkZ,
                         World world, IChunkGenerator chunkGenerator,
                         IChunkProvider chunkProvider) {
        switch (world.provider.getDimension()) {
          case -1: //Nether
              break;

          case 0: //Overworld
              runGenerator(BlockInitializer.tutorialBlock.getDefaultState(),
                           BLOCK_AMOUNT, CHANCES_TO_SPAWN, MIN_HEIGHT, MAX_HEIGHT,
                           BlockMatcher.forBlock(Blocks.STONE),
                           world, random, chunkX, chunkZ);
              break;

          case 1: //End
              break;

          default: //Everything else
              break;
        }
    }

    private void runGenerator(IBlockState blockToGen, int blockAmount, int chancesToSpawn,
                              int minHeight, int maxHeight,
                              Predicate<IBlockState> blockToReplace, World world,
                              Random rand, int chunk_X, int chunk_Z) {
        if (minHeight < 0 || maxHeight > 256 || minHeight > maxHeight) {
          throw new IllegalArgumentException("Illegal Height Arguments for WorldGenerator");
        }

        WorldGenMinable generator = new WorldGenMinable(blockToGen, blockAmount, blockToReplace);
        int heightdiff = maxHeight - minHeight +1;
        for (int i=0; i<chancesToSpawn; i++) {
          int x = chunk_X * 16 +rand.nextInt(16);
          int y = minHeight + rand.nextInt(heightdiff);
          int z = chunk_Z * 16 + rand.nextInt(16);
          generator.generate(world, rand, new BlockPos(x, y, z));
        }
    }
}